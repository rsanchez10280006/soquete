package concurrente;
/**
 *
 * @author Roberto
 */
import javax.swing.*;
import java.io.*;
import java.net.*;

public class servidorconcurrente extends JFrame {
    
    int controlclientes=2;
    ServerSocket servidor;
    Socket connecting;
    DataInputStream flujo_entrada;
    DataOutputStream flujo_salida;
    
    double numero1, numero2, suma;
    
    JTextArea areadetexto;
    
    
    public servidorconcurrente (){
      super ("servidor");  
        
        areadetexto= new JTextArea();
        add (areadetexto);
        
        setSize( 550, 400 );
        setVisible( true );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        try
        {
            
            
            
        servidor=new ServerSocket(65000);//puerto de comunicacion y el largo de la cola que aceptara
         
        servidorHilo hilo;
        
        
        do{
          areadetexto.append("servidor en linea y en espera \n");  
          hilo=new servidorHilo(servidor);  
          connecting=hilo.cliente();
          
        flujo_entrada=new DataInputStream(connecting.getInputStream());
        flujo_salida=new DataOutputStream(connecting.getOutputStream());
        
        areadetexto.append("recibido numero 1 \n");
        numero1=flujo_entrada.readDouble();
        
        areadetexto.append("recibido numero 2 \n");
        numero2=flujo_entrada.readDouble();
        
        suma=numero1+numero2;
        areadetexto.append("enviando resultado \n");
        
        flujo_salida.writeDouble(suma);
            areadetexto.append("suma enviada, cerrando conexion \n");
            connecting.close();
            
        controlclientes --;
            
        }while(controlclientes >0 && connecting != null  );
            
        
         
        }
         
        catch (Exception e)
        {
            e.getMessage();
        }
    
    
    
    
}
    public static void main(String[] args)
    {
        servidorconcurrente iniciaservidor =new servidorconcurrente();
        
    }

    
}
